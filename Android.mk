LOCAL_PATH := $(call my-dir)

apache-httpmime_src_files := \
    $(call all-java-files-under,httpcore/trunk/httpcore/src/main/java/org/apache/http) \
    $(call all-java-files-under,httpcore/trunk/httpcore/src/main/java/org/apache/http/util) \
    $(call all-java-files-under,httpcore/trunk/httpcore/src/main/java/org/apache/http/entity) \
    $(call all-java-files-under,httpclient/trunk/httpmime/src/main/java/org/apache/http/entity/mime) \
    $(call all-java-files-under,httpclient/trunk/httpmime/src/main/java/org/apache/http/entity/mime/content)

include $(CLEAR_VARS)
LOCAL_MODULE := apache-httpmime
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := $(apache-httpmime_src_files)
LOCAL_JAVACFLAGS := -encoding UTF-8
LOCAL_JAVA_LIBRARIES := core
LOCAL_NO_STANDARD_LIBRARIES := true
include $(BUILD_JAVA_LIBRARY)

ifeq ($(WITH_HOST_DALVIK),true)
    include $(CLEAR_VARS)
    LOCAL_MODULE := apache-httpmime-hostdex
    LOCAL_MODULE_TAGS := optional
    LOCAL_SRC_FILES := $(apache-httpmime_src_files)
    LOCAL_JAVACFLAGS := -encoding UTF-8
    LOCAL_JAVA_LIBRARIES := core-hostdex
    LOCAL_NO_STANDARD_LIBRARIES := true
    LOCAL_BUILD_HOST_DEX := true
    LOCAL_MODULE_TAGS := optional
    include $(BUILD_HOST_JAVA_LIBRARY)
endif

